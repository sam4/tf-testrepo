variable "stage" {
    type = string
}

variable "appname" {
  type = string
}

variable "department" {
  type = string
}

variable "azure_location_westeurope" {
    type = string
}

variable "azure_tenant_id" {
  type = string
}

variable "azure_subscription_id" {
  type = string
}

variable "grp_sec_app_readonly_id" {
  type = string
}

variable "grp_sec_app_contribute_id" {
  type = string
}

variable "grp_sec_app_admin_id" {
  type = string
}

variable "grp_sec_app_breakglass_id" {
  type = string
}

#******************************************************************
# Start here with editing: Project specific variables             *
#******************************************************************

#******************************************************************
# End here with edititng: Project specific variables              *
#******************************************************************
