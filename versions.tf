terraform {
  required_version = "~> 0.13.3"

  # enforce certain versions using pessimistic constraint operator (see also https://www.terraform.io/docs/configuration/version-constraints.html#version-constraint-syntax)
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.28.0"
    }
  }
}
