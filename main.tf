# Based on template release 20201001
# Configure the Azure Provider
provider "azurerm" {
    #version = "~> 2.13"
    features {}
}

provider "azuread" {
    version = "~> 0.8"
}

terraform {
  backend "azurerm" {
  }
}

#usually you do not need to edit this file unless you start having multiple modules or need more environment variables
#you should be coding you infrastructure as one (or multiple) modules. These are programmed via the main.tf in the
#/modules/ folder
module "main_application" {
  source = "./modules/main_application"
  appname = var.appname
  department = var.department
  stage = var.stage
  azure_location_westeurope = var.azure_location_westeurope
  azure_tenant_id = var.azure_tenant_id
  azure_subscription_id = var.azure_subscription_id
  grp_sec_app_readonly_id = var.grp_sec_app_readonly_id
  grp_sec_app_contribute_id = var.grp_sec_app_contribute_id
  grp_sec_app_admin_id = var.grp_sec_app_admin_id
  grp_sec_app_breakglass_id = var.grp_sec_app_breakglass_id

  #******************************************************************
  # Start here with editing: Project specific variables             *
  #******************************************************************
  
  #******************************************************************
  # End here with edititng: Project specific variables              *
  #******************************************************************

}